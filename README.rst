=============
PyGotham 2016
=============

The PyGotham 2016 website. This archived is released under the same license as
[PyGotham](https://github.com/PyGotham/pygotham).

This can be recreated with::

    wget --recursive --page-requisites --convert-links --no-parent --html-extension 2016.pygotham.org/
